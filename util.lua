local globals = gFunc.LoadFile('globals.lua')

local applicableBelts = {
	['Flame Belt']   = {'Liquefaction', 'Fusion'},
	['Soil Belt']    = {'Scission', 'Gravitation'},
	['Aqua Belt']    = {'Reverberation', 'Distortion'},
	['Breeze Belt']  = {'Detonation', 'Fragmentation'},
	['Snow Belt']    = {'Induration', 'Distortion'},
	['Thunder Belt'] = {'Impaction', 'Fragmentation'},
	['Light Belt']   = {'Transfixion', 'Fusion', 'Light'},
	['Shadow Belt']  = {'Compression', 'Gravitation', 'Darkness'}
}

local applicableGorgets = {
	['Flame Gorget']   = {'Liquefaction', 'Fusion'},
	['Soil Gorget']    = {'Scission', 'Gravitation'},
	['Aqua Gorget']    = {'Reverberation', 'Distortion'},
	['Breeze Gorget']  = {'Detonation', 'Fragmentation'},
	['Snow Gorget']    = {'Induration', 'Distortion'},
	['Thunder Gorget'] = {'Impaction', 'Fragmentation'},
	['Light Gorget']   = {'Transfixion', 'Fusion', 'Light'},
	['Shadow Gorget']  = {'Compression', 'Gravitation', 'Darkness'}
}

local getAccessoryForProperty = function(owned, accessoryProperties, ability, minimumLevel)
	if(gData.GetPlayer().MainJobLevel < minimumLevel) then return nil end
	if(owned['Fotia Belt']) then return 'Fotia Belt' end
	if(owned['Fotia Gorget']) then return 'Fotia Gorget' end
	skillchain = ability.skillchain
	for _, abilityProperty in pairs(skillchain) do
		for accessoryName, accessoryLatents in pairs(accessoryProperties) do
			if(owned[accessoryName]) then
				for _, accessoryProperty in pairs(accessoryLatents) do
					if (accessoryProperty == abilityProperty) then
						if (globals.debug) then print("Found "..accessoryName..' appropriate for ability '..ability.en) end
						return accessoryName
					end
				end
			end
		end
	end
	if (globals.debug) then print('Could not find appropriate accessory for property: '..ability.en) end
	return nil
end

---checks if an elemental belt with an appropriate property is owned.
---@return string?
---The first matching belt, or nil if none.
---@param belts table<string, boolean> Belts named K with boolean V Ownership
local getAppropriateBelt = function(belts, ability)
	return getAccessoryForProperty(belts, applicableBelts, ability, 87)
end

---checks if an elemental belt with an appropriate property is owned.
---@return string?
---The first matching belt, or nil if none.
---@param gorgets table<string, boolean> 
---Belts named K with boolean V Ownership
local getAppropriateGorget = function(gorgets, ability)
	return getAccessoryForProperty(gorgets, applicableGorgets, ability, 72)
end



return {
	---Handles the entire belt and gorget process
	---@param ability skillchainData
	---the ability object obtained from the skills library (Thanks Ivaar)
	---@param data playerData
	doBeltAndGorget = function(ability, data)
		local belt = getAppropriateBelt(data.ownedBelts, ability)
		local gorget = getAppropriateGorget(data.ownedGorgets, ability)
		if (belt ~= nil) then
			gFunc.Equip('Waist', belt)
		end
		if (gorget ~= nil) then
			gFunc.Equip('Neck', gorget)
		end
	end
}