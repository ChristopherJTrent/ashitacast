NOTE: This assumes you already have both Ashita V4 and LuAshitacast installed.

## Ark's LuAshitacast profiles

I've gone to a decent amount of trouble to write up my profiles, so I figured: "might as well share them."

These were made to be used on the OmicronXI private server, though if you're stuck in the stone age on retail, they'll work there too.

### Installation
You can install this manually, or with git.

#### Git - Recommended
- Use your command line of choice to navigate to Ashita/config/addons
- run `git clone https://gitlab.com/ChristopherJTrent/ashitacast.git luashitacast` in your terminal.

#### Manual
Extract the contents of the `ashitacast-master` folder inside the archive downloaded [here](https://gitlab.com/ChristopherJTrent/ashitacast/-/archive/master/ashitacast-master.zip) into your Ashita/config/addons/luashitacast folder.

### Writing your own luas
You can take my files as they are, or use the additional helper and utility functions I've written in your own luas.
#### From scratch
inside FFXI, run `/lac newlua`  
take a look inside util.lua for some helpful functions.