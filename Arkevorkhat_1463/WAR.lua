local skills = gFunc.LoadFile('skills.lua')
local util = gFunc.LoadFile('util.lua')
local data = gFunc.LoadFile('index.lua')


local profile = {};
local sets = {
	['weaponskill'] = {},
    ['idle'] = {
        Head = 'Outrider Mask',
        Neck = 'Wivre Gorget',
        Ear2 = 'Raising Earring',
        Body = 'Outrider Mail',
        Hands = 'Outrider Mittens',
        Ring1 = 'Vehemence Ring',
        Ring2 = 'Enlivened Ring',
        Back = 'Accura Cape',
        Waist = 'Headlong Belt',
        Legs = 'Outrider Hose',
        Feet = 'Outrider Greaves',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    gSettings.AllowAddSet = true;
end

profile.OnUnload = function()
end

profile.HandleCommand = function(args)
end

profile.HandleDefault = function()
	gFunc.EquipSet(sets.idle)
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
	local action = gData.GetAction();
	local set = sets.weaponskill[action.Name]
	if(set ~= nil) then 
		gFunc.EquipSet(set)
	end
	util.doBeltAndGorget(skills[3][action.Id], data)
end

return profile;