---@class playerData
---@field public ownedBelts table<string, boolean>
---@field public ownedGorgets table<string, boolean>
return {
	ownedBelts = {
		['Flame Belt']   = false,
		['Soil Belt']    = false,
		['Aqua Belt']    = false,
		['Breeze Belt']  = true,
		['Snow Belt']    = false,
		['Thunder Belt'] = false,
		['Light Belt']   = true,
		['Shadow Belt']  = false,
		['Fotia Belt']   = false
	},
	ownedGorgets = {
		['Flame Gorget']   = false,
		['Soil Gorget']    = false,
		['Aqua Gorget']    = false,
		['Breeze Gorget']  = false,
		['Snow Gorget']    = false,
		['Thunder Gorget'] = false,
		['Light Gorget']   = false,
		['Shadow Gorget']  = false,
		['Fotia Gorget']   = false
	},
}